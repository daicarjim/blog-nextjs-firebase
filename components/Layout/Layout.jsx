import { signOut } from '@lib/firebase';
import { useAuth } from '@contexts/auth';
import styles from './Layout.module.scss';

const Layout = ({ children }) => {
  const [user] = useAuth();

  return (
    <div className={styles.Layout}>
      <nav>
        <span>
          <a href="/">Daimon Cardenas Developer</a>
        </span>
        {user && (
        <span>
            <a href="/create">create</a>
          <span>
            <button onClick={() => signOut()}>Sign Out</button>
          </span>
        </span>  
        )}
         {user == null && (
          <span>
            <a href="/signin">Sign In</a>
          </span>
        )}
      </nav>
      <main>{children}</main>
    </div>
  );
};

export default Layout;
